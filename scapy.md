efcp = 3671
ip = IP(src="192.168.0.23", dst="192.168.0.11")
udp = UDP(sport=efcp, dport=efcp)

import codecs
decoder = codecs.getdecoder("hex_codec")

data_str = "".join("06:10:05:30:00:13:29:00:b4:d0:10:0d:40:29:03:00:80:36:1b".split(":"))
data = decoder(data_str)[0]

packet= ip/udp/Raw(load=data)
packet.show()

send(packet)
