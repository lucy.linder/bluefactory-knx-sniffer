#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from sknx.converter import *

"""
to ensure the filter works correctly, properly install all the optional
dependencies:

  sudo apt-get install tcpdump graphviz imagemagick python-gnuplot python-crypto python-pyx


"""
__author__ = 'lucy linder <lucy.derlin@gmail.com>'

CAPTURE_IFACE = "eth0"
CAPTURE_FILTER = "udp port 3671"


def got_packet(packet):
    """
    method called for each packet captured by the scapy sniffer.
    the sniffer must be configured to filter KNXNet/IP packets only
    :param packet: the udp packet containing an KNXNet/IP application frame
    """

    # parse the knx frame
    knx = KNX(packet.load)

    # print the knx frame
    knx.show()
    print("\n     type      = %s\n" % KnxServiceTypeIdentifier.fromValue(knx.service))

    # check the length
    if len(knx.load) != knx.len - knx.hlen:
        print("MISMATCH : len(knx.load) != knx.len - knx.hlen")
        return

    # parse the cEMI frame
    cemi = CEMI(knx.load)

    # print cemi
    cemi.show()
    print("  saddr fmt = %s" % int_to_address(cemi.saddr))
    print("  daddr fmt = %s\n" % int_to_group_address(cemi.daddr))
    print("###[ DATA ]###")
    print("     hex    : 0x", bytes_to_hex_string(cemi.load, delim=" 0x"))
    print("     decimal: ", bytes_to_decimal_string(cemi.load))

    print(convert(cemi))


if __name__ == "__main__":

    iface = CAPTURE_IFACE
    if len(sys.argv) > 1: iface = sys.argv[1]

    sniff(iface=iface,
          prn=got_packet,
          filter=CAPTURE_FILTER,
          store=0)
