#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* util to convert a numeric define to a string */
/* cf: http://stackoverflow.com/questions/5459868/c-preprocessor-concatenate-int-to-string */
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/* max bytes per packet to capture */
#define CAP_LEN 1518

/* ethernet and udp headers have always the same length */
#define ETHERNET_HEADER_LEN  14
#define UDP_HEADER_LEN        8

/* Ethernet header
struct ethernet_header {
    u_char  ether_dhost[ETHER_ADDR_LEN];    // destination host address
    u_char  ether_shost[ETHER_ADDR_LEN];    // source host address
    u_short ether_type;                     // IP? ARP? RARP? etc
};
*/

/* IP header */

#define IP_FRAG_RESERVED 0x8000     // reserved fragment flag
#define IP_FRAG_DONT     0x4000     // dont fragment flag
#define IP_FRAG_MORE     0x2000     // more fragments flag
#define IP_FRAG_OFFSET   0x1fff     // mask for fragmenting bits

#define IP_HEADER_LENGTH(ip)   ((((ip)->v_hl) & 0x0f)*4)
#define IP_VERSION(ip)         (((ip)->v_hl) >> 4)

struct ip_header {
    u_char v_hl;          // version  + header length
    u_char tos;           // type of service
    u_short total_len;    // total length
    u_short id;           // identification
    u_short frag_offset;  // fragment offset field
    u_char ttl;           // time to live
    u_char proto;         // protocol
    u_short checksum;     // checksum
    struct in_addr src;   // source and dest address
    struct in_addr dest;  // source and dest address
};


/* UDP header */
struct udp_header {
    u_short src_port;    // source port
    u_short dest_port;   // destination port
    u_short total_len;   // datagram length
    u_short checksum;    // datagram checksum
};

/* KNX header */
#define KNX_PORT 3671
#define KNX_HEADER_LENGTH  6

struct knx_header {
    u_char hl;            // header len, 1 byte
    u_char proto;        // protocol 1 byte => currently 1.0, transmitted as 0x6
    u_short service;    // service type, 2 bytes
    u_short total_len;  // total length, 2 bytes
};

#define KNX_PAYLOAD_LENGTH(knx)   (ntohs((knx)->total_len)-((knx)->hl))


/*------------- declarations */

/* handler/listener called by pcap */
void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);

void knx_parse(const u_char *udp_payload, const int offset);

void knx_dump_header(const struct knx_header *knx);

void dump_payload(const u_char *payload, int len);

/*------------- definitions */

void knx_parse(const u_char *udp_payload, const int len) {

    const struct knx_header *knx;
    const u_char *payload;
    int payload_len;

    knx = (struct knx_header *) (udp_payload);
    payload = (u_char *) (udp_payload + KNX_HEADER_LENGTH);
    payload_len = KNX_PAYLOAD_LENGTH(knx);

    if (payload_len != len - KNX_HEADER_LENGTH) {
        printf("!! inconsistency: hl: %x, total length: %x VS udp payload: %x",
               knx->hl, ntohs(knx->total_len), len);
        return;
    }

    // debug
    printf("\nKNX Packet (len: %d)\n", len);
    knx_dump_header(knx);
    printf("\n");
    dump_payload(payload, payload_len);
    printf("\n");
}

void knx_dump_header(const struct knx_header *knx) {
    printf("  header length: %d\n", knx->hl);
    printf("  protocol     : %d\n", knx->proto);
    printf("  service type : %x\n", ntohs(knx->service));
    printf("  total length : %d\n", ntohs(knx->total_len));
}

void dump_payload(const u_char *payload, int len) {
    int offset = 0, i = 0;
    while (offset < len) {
        printf("%02x ", payload[offset++]);
        if (++i >= 8) {
            printf("\n");
            i = 0;
        }
    }
    printf("\n");
}

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {

    const struct ip_header *ip;
    const struct udp_header *udp;

    int ip_hlen;
    const char *src_addr, *dest_addr;
    uint16_t src_port, dest_port;

    const u_char *udp_payload;
    int udp_payload_len;

    /* decapsulate IP */

    ip = (struct ip_header *) (packet + ETHERNET_HEADER_LEN);

    ip_hlen = IP_HEADER_LENGTH(ip);
    src_addr = inet_ntoa(ip->src);
    dest_addr = inet_ntoa(ip->dest);

    if (ip_hlen < 20) {
        fprintf(stderr, "Error: invalid IP header length: %u bytes (%s => %s)\n",
                ip_hlen, src_addr, dest_addr);
        return;
    }

    if (ip->proto != IPPROTO_UDP) {
        printf(" Info: got a non-udp packet(%s => %s)\n", src_addr, dest_addr);
        return;
    }

    /* decapsulate UDP */
    udp = (struct udp_header *) (packet + ETHERNET_HEADER_LEN + ip_hlen);
    src_port = ntohs(udp->src_port);
    dest_port = ntohs(udp->dest_port);


    // debug
    printf("%s:%d => \n%s:%d\n", src_addr, src_port, dest_addr, dest_port);

    if (src_port != KNX_PORT && dest_port != KNX_PORT) {
        printf(" Info: got a non-knx packet(%s => %s)\n", src_addr, dest_addr);
        return;
    }

    udp_payload = (u_char *) (packet + ETHERNET_HEADER_LEN + ip_hlen + UDP_HEADER_LEN);
    udp_payload_len = ntohs(udp->total_len) - UDP_HEADER_LEN;

    /* handle KNX */
    knx_parse(udp_payload, udp_payload_len);

}


/*------------- main  */

int main(int argc, char **argv) {


    char filter_exp[] = "udp port "STR(KNX_PORT);  // filter expression

    char *dev = NULL;                // capture device name
    char errbuf[PCAP_ERRBUF_SIZE];   // error buffer
    pcap_t *handle;                  // packet capture handle
    struct bpf_program fp;  // compiled filter program (expression)
    bpf_u_int32 mask = 0;   // subnet mask
    bpf_u_int32 net = 0;    // ip
    int num_packets = -1;   // number of packets to capture (-1 : no limit)


    /* find a capture device */
    dev = pcap_lookupdev(errbuf);
    if (dev == NULL) {
        fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
        exit(EXIT_FAILURE);
    }


    /* get network number and mask associated with capture device */
    if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
        fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuf);
        net = 0;
        mask = 0;
    }

    /* print capture info */
    printf("Device: %s, net: %x, mask: %x\n\n", dev, net, mask);

    /* open capture device */
    handle = pcap_open_live(dev, CAP_LEN, 1, 1000, errbuf);
    if (handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
        exit(EXIT_FAILURE);
    }

    /* make sure we're capturing on an Ethernet device */
    if (pcap_datalink(handle) != DLT_EN10MB) {
        fprintf(stderr, "%s is not an Ethernet\n", dev);
        exit(EXIT_FAILURE);
    }

    /* compile the filter expression */
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
        fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
        exit(EXIT_FAILURE);
    }

    /* apply the compiled filter */
    if (pcap_setfilter(handle, &fp) == -1) {
        fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
        exit(EXIT_FAILURE);
    }

    /* now we can set our callback function */
    pcap_loop(handle, num_packets, got_packet, NULL);

    /* cleanup */
    pcap_freecode(&fp);
    pcap_close(handle);

    printf("\nCapture complete.\n");

    return 0;
}
