#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from sknx.knx import *
import json
import sqlite3
import logging
from collections import namedtuple

__author__ = 'lucy'


"""
Logging
"""
logger = logging.getLogger('knx-bb')

"""
Configuration
"""

def json_to_obj(json_str):
    """ Parse JSON into an object with attributes corresponding to dict keys.
    :param json_str: the json string
    :return: the namedtuple (read-only)
    """
    return json.loads(json_str, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))



def load_config(file="config.json"):
    """
    load the json configuration file into a namedtuple
    :param file: the path to the configuration file
    :return the configuration object
    """
    with open(file) as f:
        return json_to_obj(f.read())


config = load_config()

"""
Conversion functions
"""

def convert(cemi):
    """
    convert a cEMI payload to a json compatible with the BBData input API
    :param cemi: the cEMI frame
    :return: the json string, or None if an error occurred
    """
    ts = curtime()
    address = int_to_address(cemi.saddr)

    if is_blacklisted(address): return False

    value = cemi.hex_data()
    return json.dumps({"id": config.id, "token": config.token, "address": address, "value": value, "timestamp": ts})


def is_blacklisted(addr):
    """
    check if the address is blacklisted
    :param addr: the address to look up
    :return: true is blacklisted, false otherwise
    """
    return addr in config.blacklist


def curtime():
    """
    :return: string, the current time in the RFC3339 format
    """
    return datetime.now(timezone.utc).astimezone().isoformat()

