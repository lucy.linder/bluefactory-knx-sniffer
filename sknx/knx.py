#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scapy.all import *
from enum import Enum
from re import findall
from binascii import hexlify

__author__ = 'lucy linder <lucy.derlin@gmail.com>'

"""
cEMI header message code field (see @CEMI)
1 byte at offset 0
"""
CEMI_MSG_CODES = {
    0x10: "L_Raw.req",
    0x11: "L_Data.req",
    0x13: "L_Poll_Data.req",
    0x25: "L_Poll_Data.con",
    0x29: "L_Data.ind",
    0x2B: "L_Busmon.ind",
    0x2D: "L_Raw.ind",
    0x2E: "L_Data.con",
    0x2F: "L_Raw.con",
}


class KnxServiceTypeIdentifier(Enum):
    """
    KNX Service Type Identifier, 2 bytes at offset 2
    Use the KnxServiceTypeIdentifier.fromValue method to create
    an object from the raw binary code.
    """
    Core = 0x0200
    Device_Management = 0x0310
    Tunnelling = 0x0420
    Routing = 0x0530
    Remote_Logging = 0x0600
    Remote_Configuration_and_Diagnosis = 0x0740
    Object_Server = 0x0800

    @staticmethod
    def fromValue(val):
        """
        create a KnxServiceTypeIdentifier object from a short unsigned integer
        :param val: the short integer
        :return: a KnxServiceTypeIdentifier enum object
        """
        if 0x0200 <= val <= 0x020F: return KnxServiceTypeIdentifier.Core
        if 0x0310 <= val <= 0x031F: return KnxServiceTypeIdentifier.Device_Management
        if 0x0420 <= val <= 0x042F: return KnxServiceTypeIdentifier.Tunnelling
        if 0x0530 <= val <= 0x053F: return KnxServiceTypeIdentifier.Routing
        if 0x0600 <= val <= 0x06FF: return KnxServiceTypeIdentifier.Remote_Logging
        if 0x0740 <= val <= 0x07FF: return KnxServiceTypeIdentifier.Remote_Configuration_and_Diagnosis
        if 0x0800 <= val <= 0x08FF: return KnxServiceTypeIdentifier.Object_Server


class KNX(Packet):
    """
    A custom scapy @Packet for dealing with KNXNet/IP packets.
    Example use: knx = KNX(udp.load)
    """
    name = "KNXNet/IP Packet"

    fields_desc = [
        # header length is always 6 B
        XByteField("hlen", 6),
        # protocol version, currently 1.0, encoded 0x10
        XByteField("proto", 10),
        # service type identifier (see @KnxServiceTypeIdentifier)
        XShortField("service", 0),
        # total length (header+payload)
        ShortField("len", 0)]


class CEMI(Packet):
    """
    A custom scapy @Packet for dealing with cEMI
    (Common External Message Interface ) packets.
    Example use: cemi = CEMI(knx.load)
    """
    name = "cEMI Message"

    fields_desc = [
        # message code
        ByteEnumField("msCode", 0, CEMI_MSG_CODES),

        # additional info length + data
        BitFieldLenField("aiLen", None, 8, length_of="ai"),
        StrLenField("ai", "", length_from=lambda pkt: pkt.aiLen),

        # control field 1
        FlagsField("ctrl", 0, 8, ['L_Data.confirm', 'L_Data.req', 'priority_MSB',
                                  'priority_LSB', 'sys_broadcast', 'Repeat', '',
                                  'Frame Type']),
        # control field 2
        BitField("daddr_type", 0, 1),
        BitField("hop_count", 0, 3),
        BitField("extended", 0, 4),

        # src and dst addresses TODO formatting
        XShortField("saddr", 0),
        XShortField("daddr", 0),

        # NPDU
        # XByteField("NPDU_len", 0)
    ]

    def hex_data(self):
        return hexlify(self.load).decode("utf-8")


def int_to_address(short):
    """
    format a short integer into a KNX individual address string.
    the KNX individual address format is :
        4 bit = Area, 4 bit = Line, 8 bits = Bus device
    example: int_to_address(0x100d) => 1.0.13

    :param short: the integer representing the address
    :return: string, the formatted address
    """
    area = (short & 0xF000) >> 12
    line = (short & 0x0F00) >> 8
    bus = (short & 0x00FF)

    return "%d.%d.%d" % (area, line, bus)


def int_to_group_address(short):
    """
    format a short integer into a KNX group address string.
    the KNX group address format is not well defined. There are two alternatives.
    1) 2-level: 5 bits = Main group, 11 bits = Subgroup
    1) 3-level: 5 bits = Main group, 3 bits: Middle group, 8 bits = Subgroup
    this method uses the 2-level (1) format.
    example: int_to_group_address(0x4029) => 8/41

    :param short: the integer representing the address
    :return: string, the formatted address
    """
    maingrp = (short & 0xF800) >> 11
    subgrp = (short & 0x07FF)
    return "%d/%d" % (maingrp, subgrp)


def bytes_to_hex_string(bs, delim=" "):
    """
    convert a binary string into a printable hex string.

    :param bs: the binary string
    :param delim: the delimiter, inserted between each group of four bytes (" " by default)
    :return: the formatted hex string
    """
    groups = findall("..", hexlify(bs).decode("utf-8"))
    return delim.join(groups)


def bytes_to_decimal_string(bs, delim=" "):
    """
    convert a binary string into a printable string of short integers.

    :param bs: the binary string
    :param delim: the delimiter, inserted between each group of four bytes (" " by default)
    :return: the formatted decimal string
    """
    groups = findall("..", hexlify(bs).decode("utf-8"))
    decimals = [int(x, 16) for x in groups]
    return delim.join([str(x) for x in decimals])
