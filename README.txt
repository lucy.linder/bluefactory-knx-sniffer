# run on MAC OSX:

Install scapy and all its dependencies:

    brew install libnet libdnet
    sudo pip3 install scapy-python3 netifaces

Then, don't forget to:

    1) add the source files (sknx) to the PYTHONPATH: export PYTHONPATH=...
    2) use sudo to avoid segmentation fault
    3) if the program returns straight away, check if you passed the correct interface
       (default to eth0, but won't work on Mac OSX)


# troubleshooting on Linux

If the filter doesn't work, try to install all the optional dependencies:

    sudo apt-get install tcpdump graphviz imagemagick python-gnuplot python-crypto python-pyx

